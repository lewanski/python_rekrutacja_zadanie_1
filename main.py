from time import sleep
from os import system

# Dimensions
HEIGHT = 14
WIDTH = 14

# Possible states
DEAD = 0
ALIVE = 1

def new_area():
    return [[DEAD for _ in range(WIDTH)] for _ in range(HEIGHT)]

# For sake of code clarity, I decided not to use print with two str-joins inside
def print_area(area):
    for y, h in enumerate(area):
        for x in range(len(h)):
            value = area[y][x]
            print(f"\x1b[0;30;43m{value}\x1b[0m" if value == ALIVE else value, end=" ")
        print()

# Simple data validation added, this implementation also works for area corners and edges.
# And it is possible to specify another parameter to change the neighborhood size
def _count_alived_neighborhood(area, x, y):
    len_y = len(area)
    if len_y < 1:
        raise ValueError("Invalid parameter area")

    len_x = len(area[0])
    if len_x < 1:
        raise ValueError("Invalid parameter area")

    if not (0 <= x < len_x and 0 <= y < len_y):
        raise IndexError("Invalid coordinates for a cell")

    neighborhood = []
    for delta_y in range(-1, 2, 1):
        for delta_x in range(-1, 2, 1):
            if delta_x == 0 and delta_y == 0:
                continue

            neighborhood.append(area[(y + delta_y) % len_y][(x + delta_x) % len_x])

    return neighborhood.count(ALIVE)

# For these two functions I placed whole logic here, instead of having it distributed
def _should_be_resurrected(area, x, y):
    return area[y][x] == DEAD and _count_alived_neighborhood(area, x, y) == 3

def _should_be_killed(area, x, y):
    return area[y][x] == ALIVE and _count_alived_neighborhood(area, x, y) not in [2, 3]

# Removed else-statement and added ValueError to except
def calculate_next_field_state(area, x, y):
    try:
        if _should_be_resurrected(area, x, y):
            return ALIVE
        elif _should_be_killed(area, x, y):
            return DEAD
        return area[y][x]
    except (IndexError, ValueError):
        return None

# Removed duplicated code, also fixed the if-statement to check against None (since we are operating on 0's and 1's)
def process_next_generation(area, new_area_generator):
    new_area = new_area_generator()
    print_area(area)

    for y, h in enumerate(area):
        for x in range(len(h)):
            new_state = calculate_next_field_state(area, x, y)
            if new_state is not None:
                new_area[y][x] = new_state
    print()
    return new_area

# Moved the actual executed code here, introduced slicing
start_area = new_area()
start_area[5][5:8] = [ALIVE, ALIVE, ALIVE]
start_area[6][5:8] = [ALIVE, DEAD, ALIVE]
start_area[7][5:8] = start_area[6][5:8]
start_area[8][5:8] = start_area[5][5:8]

while True:
    system("clear")
    start_area = process_next_generation(start_area, new_area)
    sleep(1)